# from django.shortcuts import render
from blog.models import Article 
from .serializers import ArticleSerializer,UserSerializer
from rest_framework.generics import ListCreateAPIView ,RetrieveUpdateDestroyAPIView
from django.contrib.auth.models import User
# Create your views here.

class ArticleList(ListCreateAPIView):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer

class ArticleDetial(RetrieveUpdateDestroyAPIView):
    queryset=Article.objects.all()
    serializer_class=ArticleSerializer

class UserList(ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class UserDetial(RetrieveUpdateDestroyAPIView):
    queryset=User.objects.all()
    serializer_class=UserSerializer