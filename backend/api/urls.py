from django.urls import path 
from .views import ArticleList,ArticleDetial,UserDetial,UserList

app_name = 'api'
urlpatterns = [
    path('',ArticleList.as_view(), name='list'),
    path('<int:pk>',ArticleDetial.as_view(), name='detial'),
    path('user/',UserList.as_view(), name='user-list'),
    path('user/<int:pk>',UserDetial.as_view(), name='user-detial'),

]
